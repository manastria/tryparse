/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.manastria.tryParse;



/**
 *
 * @author Jean-Philippe
 */
public class TryParse {

    public static boolean tryParseInteger(String value, Integer result, Integer defaultValue) {
        result = defaultValue;

        if (value == null) {
            return true;
        }

        try {
            result = Integer.parseInt(value.trim());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


    public static boolean tryParseDouble(String value, Double result, Double defaultValue) {
        result = defaultValue;

        if (value == null) {
            return true;
        }

        try {
            result = Double.parseDouble(value.trim());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
