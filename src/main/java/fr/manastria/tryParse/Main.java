/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.manastria.tryParse;

import static fr.manastria.tryParse.TryParse.tryParseDouble;
import static fr.manastria.tryParse.TryParse.tryParseInteger;
import java.text.MessageFormat;

/**
 *
 * @author Jean-Philippe
 */
public class Main {

    public static void main(String[] args) {
        String[] values = {null, "160519", "9432.0", "16,667",
            "   -322   ", "+4302", "(100);", "01FA"};

        for(String v : values) {
            Integer r = null;

            boolean result = tryParseInteger(v, r, null);

            if (result) {
                System.out.println(MessageFormat.format("OK : {0} -> {1}", v, r));
            } else {
                System.out.println(MessageFormat.format("-- : {0} -> {1}", v, r));
            }
        }

        System.out.println("----------------------------------------------------------------");

        for(String v : values) {
            Double r = null;

            boolean result = tryParseDouble(v, r, null);

            if (result) {
                System.out.println(MessageFormat.format("OK : {0} -> {1}", v, r));
            } else {
                System.out.println(MessageFormat.format("-- : {0} -> {1}", v, r));
            }
        }
    }
}
